export const ROUTES = {
  // Common Routes
  HOME: '/',
  NOT_FOUND: '/404',
  LOGIN: '/login',
  REGISTER: '/register',
  REGISTRATION_STATUS: '/registration/status',
  // User Related Routes
  DASHBOARD_USER: '/user/dashboard',
  BOOKS: '/books/:is_issue_option',
  REQUEST_BOOK: '/request-book',
  // Admin Related Routes
  DASHBOARD_ADMIN: '/admin/dashboard',
  USERS: '/users',
  REGISTRATIONS: '/registrations',
  // Super Admin Related Routes
  DASHBOARD_SUPER_ADMIN: '/super-admin/dashboard',
  BRANCH_ADD: '/branch/add',
  BRANCH_DETAILS: '/branch/:branch_id',
  ADMIN_ADD: '/admin/add',
  BOOKS_ADD: '/books/add',
  // Common Routes For Admin and Super Admin
  REQUESTS: '/requests',
};