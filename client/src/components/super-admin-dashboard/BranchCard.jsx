import React from "react";
import { Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

const BranchCard = ({ branch }) => {
  const { name, admin, _id: branchId } = branch;
  return (
    <div className="branch-card">
      <h1 className="branch-name mb-4">{name}</h1>
      <h4 className="branch-admin mb-2">{admin.name}</h4>
      <Button variant="grey" className="m-3">
        <Link className="navbar-link" to={`/branch/${branchId}`}>View</Link>
      </Button>
    </div>
  );
};

export default BranchCard;
