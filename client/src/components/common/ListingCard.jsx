import React from 'react';
import { Button, Col, Row } from 'react-bootstrap';

const ListingCard = (({ header, details, listingId, action1, action2, action1Text, action2Text, classes }) => {
  const customClass = classes && classes !== "" ? classes : "";
  return (
    <div className={`listing-card ${classes}`}>
      <Row>
        <Col sm="9" className="listing-card-details">
          <h3>{header}</h3>
          <div>
            {
              Object.keys(details).map((key, index) => {
                return (
                  <span key={index}>
                    <span className="listing-detail-heading">
                      {key}
                    </span>
                    <span>
                      : {details[key]}
                    </span>
                    <div className="separator"></div>
                  </span>
                )
              })
            }
          </div>
        </Col>
        <Col sm="3">
          {
            (action1Text) &&
            <Button className="mr-3" variant="grey" onClick={() => action1(listingId)}>{action1Text}</Button>
          }
          {
            (action2Text) &&
            <Button variant="grey" onClick={() => action2(listingId)}>{action2Text}</Button>
          }
        </Col>
      </Row>
    </div>
  )
});

export default ListingCard;
