import React from "react";
import { Col, Row } from "react-bootstrap";
import StatusCard from "./StatusCard";

const StatusIndicator = ({ status }) => {
  const { status: statusText, isUserCreated } = status;
  const finalStatus = statusText === "application approved" || statusText === "application rejected" ? statusText : "";
  return (
    <>
      <h2 className="mb-3">Application Status</h2>
      <Row className="mb-5">
        <Col sm="4">
          <StatusCard
            statusText="application sent"
            fontIcon={statusText.toLowerCase() === "application sent" ? "check-circle" : "exclamation-circle"}
          />
        </Col>
        <Col sm="4">
          <StatusCard
            statusText="application viewed"
            fontIcon={statusText.toLowerCase() === "application viewed" ? "check-circle" : "exclamation-circle"}
          />
        </Col>
        <Col sm="4">
          <StatusCard
            statusText={finalStatus !== "" ? finalStatus : "approval pending"}
            fontIcon={finalStatus === "" ? "exclamation-circle" : finalStatus === "application approved" ? "check-circle" : "times-circle" }
          />
        </Col>
      </Row>
      {isUserCreated &&
        <h3>User created. Check e-mail for password!</h3>
      }
    </>
  );
};

export default StatusIndicator;