import React from 'react';

const StatusCard = (({ statusText, fontIcon }) => {
    return (
        <div className="status-card">
            <h1 className="status-icon mb-4"><i className={`fas fa-${fontIcon}`}></i></h1>
            <h4 className="status-text mb-2">{statusText}</h4>
        </div>
    )
});

export default StatusCard;
