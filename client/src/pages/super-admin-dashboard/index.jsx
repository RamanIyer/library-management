import React, { useEffect, useState } from 'react';
import axios from 'axios';
import BranchCard from '../../components/super-admin-dashboard/BranchCard';
import { Button, Col, Row } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import StatsCard from '../../components/common/StatsCard';

const SuperAdminDashboardPage = () => {
  const [branches, setBranches] = useState([]);
  const [adminCount, setAdminCount] = useState(0);
  const [bookCount, setBookCount] = useState(0);

  useEffect(() => {
    let newBranches = [];
    axios.get("/api/branches/getAll")
      .then(res => {
        const branchData = res.data;
        branchData.forEach(branch => {
          const adminId = branch.admin;
          let newBranchData = { ...branch };
          axios.get(`/api/users/getUserById/${adminId}`)
            .then(res => {
              newBranchData.admin = res.data;
              newBranches = [...newBranches, newBranchData];
              setBranches(newBranches);
            });
        });
      });
    
    axios.get("/api/statistics/adminCount")
      .then(res => setAdminCount(res.data));
    
    axios.get("/api/statistics/allBookCount")
      .then(res => setBookCount(res.data));
  }, []);

  return (
    <div className="dashboard-page">
      <h1 className="page-heading mt-3 mb-4">Dashboard</h1>
      <h3>Branch Details</h3>
      <div className="branch-cards-holder">
        {
          branches.map((branch, index) => {
            return <BranchCard key={index} branch={branch} />
          })
        }
        <div className="branch-card">
          <h1 className="page-heading mt-3 mb-4"><i className="fas fa-plus-square"></i></h1>
          <h3><Link className="navbar-link" to="/branch/add">Add Branch</Link></h3>
        </div>
      </div>
      <h3 className="mb-5">Book Request Details</h3>
      <Row className="mt-5 mb-5">
        <Col sm="6">
          <StatsCard count="19" entity="Book Requests" message="Check it Out Soon!." />
        </Col>
        <Col sm="6">
          <Button variant="black" className="m-3">
            <Link className="navbar-link" to="/requests">View Requests</Link>
          </Button>
        </Col>
      </Row>
      <h3 className="mb-5">Admin Details</h3>
      <Row className="mt-5 mb-5">
        <Col sm="6">
          <StatsCard count={adminCount} entity="Admins" message="Working In The Library." />
        </Col>
        <Col sm="6">
          <Button variant="black" className="m-3">
            <Link className="navbar-link" to="/admin/add">Add A New Admin</Link>
          </Button>
        </Col>
      </Row>
      <h3 className="mb-5">Book Details</h3>
      <Row className="mt-5 mb-5">
        <Col sm="6">
          <StatsCard count={bookCount} entity="Books" message="Currently In The Library." />
        </Col>
        <Col sm="6">
          <Button variant="black" className="m-3">
            <Link className="navbar-link" to="/books/add">Add More Books</Link>
          </Button>
        </Col>
      </Row>
    </div>
  );
};

export default SuperAdminDashboardPage;
