import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { Button, Form, Modal } from 'react-bootstrap';
import { useHistory } from 'react-router-dom';

const AddBooksPage = () => {
  const history = useHistory();
  const [errors, setErrors] = useState({});
  const [name, setName] = useState("");
  const [author, setAuthor] = useState("");
  const [isbn, setIsbn] = useState("");
  const [genre, setGenre] = useState("");
  const [quantity, setQuantity] = useState("");
  const [branch, setBranch] = useState("Branch");
  const [branches, setBranches] = useState([]);
  const [show, setShow] = useState(false);

  useEffect(() => {
    axios.get("/api/branches/getAll")
      .then(res => {
        const branchData = res.data.map((branch) => {
          return {
            value: branch._id,
            label: branch.name,
          }
        });
        setBranches(branchData);
      });
  }, []);

  const handleClose = () => {
    setShow(false);
    setName("");
    setAuthor("");
    setIsbn("");
    setGenre("");
    setQuantity("");
    setBranch("Branch");
  };

  const addBook = () => {
    const newBook = {};
    newBook.name = name;
    newBook.author = author;
    newBook.isbn = isbn;
    newBook.genre = genre;
    newBook.quantity = quantity;
    newBook.branch = branch;

    axios.post("/api/books/addBooks", newBook)
      .then(res => {
        setShow(true);
      })
      .catch(err => setErrors(err.response.data));
  };

  return(
    <>
      <div className="add-books-page">
        <h1 className="page-heading mt-3 mb-4">Add Books</h1>
        <input className="input-field" placeholder="Book Name" value={name} onChange={(e) => setName(e.target.value)} />
        <div className="errors">
          {errors.name}
        </div>
        <input className="input-field" placeholder="Author" value={author} onChange={(e) => setAuthor(e.target.value)} />
        <div className="errors">
          {errors.author}
        </div>
        <input className="input-field" placeholder="ISBN" value={isbn} onChange={(e) => setIsbn(e.target.value)} />
        <div className="errors">
          {errors.isbn}
        </div>
        <input className="input-field" placeholder="Genre" value={genre} onChange={(e) => setGenre(e.target.value)} />
        <div className="errors">
          {errors.genre}
        </div>
        <input type="number" className="input-field" placeholder="Quantity" value={quantity} onChange={(e) => setQuantity(e.target.value)} />
        <div className="errors">
          {errors.quantity}
        </div>
        <Form.Control as="select" className="input-field" value={branch} onChange={(e) => setBranch(e.target.value)}>
          <option disabled selected>Branch</option>
          {
            branches.map((branch, index) => {
              return (
                <option key={index} value={branch.value}>{branch.label}</option>
              );
            })
          }
        </Form.Control>
        <div className="errors">  
          {errors.branch}
        </div>
        <p><Button variant="black" onClick={addBook}>Add Book</Button></p>
        <p><Button variant="black" onClick={() => {history.push("/super-admin/dashboard")}}>Back</Button></p>
      </div>
      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Book Added Successfully</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <p>The book has been added successfully to the branch!</p>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="black" onClick={handleClose}>
            Close
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
};

export default AddBooksPage;
