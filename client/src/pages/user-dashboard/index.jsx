import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { Button, Col, Modal, Row } from 'react-bootstrap';
import { Link, useHistory } from 'react-router-dom';
import ListingCard from '../../components/common/ListingCard';
import StatsCard from '../../components/common/StatsCard';

const UserDashboardPage = () => {
  const history = useHistory();
  const [issue, setIssue] = useState(null);
  const [issuedBook, setIssuedBook] = useState(null);
  const [bookCount, setBookCount] = useState(0);
  const [showRenew, setShowRenew] = useState(false);
  const [renewTitle, setRenewTitle] = useState("");
  const [renewBody, setRenewBody] = useState("");
  const [showReturn, setShowReturn] = useState(false);
  const [returnTitle, setReturnTitle] = useState("");
  const [returnBody, setReturnBody] = useState("");
  const [showNotification, setShowNotification] = useState(false);
  const [notificationTitle, setNotificationTitle] = useState("");
  const [notificationBody, setNotificationBody] = useState("");
  const { _id: userId, branch: branchId } = JSON.parse(localStorage.getItem("currentUser"));

  useEffect(() => {
    axios.get(`/api/issues/getCurrentBook/${userId}`)
      .then(res => {
        const currentIssue = res.data;
        if (currentIssue) {
          axios.get(`/api/books/getBookById/${currentIssue.book}`)
            .then(res => {
              setIssuedBook(res.data);
              setIssue(currentIssue);
            });
        }
      });
    
    axios.get(`/api/statistics/bookCount/${branchId}`)
      .then(res => setBookCount(res.data));
  }, [userId, branchId]);

  const handleRenewClose = (() => {
    setShowRenew(false);
  });

  const handleReturnClose = (() => {
    setShowReturn(false);
  });

  const handleNotificationClose = (() => {
    setShowNotification(false);
    axios.get(`/api/issues/getCurrentBook/${userId}`)
      .then(res => {
        const issue = res.data;
        setIssue(issue);
        if (issue) {
          axios.get(`/api/books/getBookById/${issue.book}`)
            .then(res => {
              setIssuedBook(res.data);
            });
        }
      });
  });

  const showRenewModal = (() => {
    setRenewTitle("Renew Book");
    setRenewBody("Are you sure you want to renew the book for 3 more days?");
    setShowRenew(true);
  });

  const renewBook = (() => {
    setShowRenew(false);
    axios.post(`/api/issues/renewBook/${issue._id}`)
      .then((res) => {
        console.log(res);
        setNotificationTitle("Renew Book");
        setNotificationBody(`Your book was renewed successfully!`);
        setShowNotification(true);
      });
  });

  const showReturnModal = (() => {
    setReturnTitle("Return Book");
    setReturnBody("Are you sure you want to return the book?");
    setShowReturn(true);
  });

  const returnBook = (() => {
    setShowReturn(false);
    axios.post(`/api/issues/returnBook/${issue._id}`)
      .then(() => {
        setNotificationTitle("Return Book");
        setNotificationBody("Your book was returned successfully.");
        setShowNotification(true);
      });
  });

  const handleIssue = (() => {
    history.push(`/books/${true}`);
  });

  return (
    <>
      <div className="dashboard-page">
        <h1 className="page-heading mt-3 mb-4">Dashboard</h1>
        <h3 className="mb-5">Current Issue Details</h3>
        <Row className="mt-5 mb-5">
          <Col sm="12">
            {issue ? (
              <ListingCard
                header={issuedBook.name}
                details={{ due: issue.issuedTill }}
                classes="book-issue"
              />
            ) : (
                <div>No Book Has Been Issued!</div>
              )
            }
          </Col>
        </Row>
        { issue ? (
          <>
            <Button variant="black" className="mr-3 mb-3" onClick={showRenewModal}>Renew</Button>
            <Button variant="black" className="mb-3" onClick={showReturnModal}>Return</Button>
          </>
        ) : (
            <Button variant="black" className="mb-3" onClick={handleIssue}>Issue</Button>
          )
        }
        <h3 className="mb-5">Books</h3>
        <Row className="mt-5 mb-5">
          <Col sm="6">
            <StatsCard count={bookCount} entity="Books" message="Currently in the library" />
          </Col>
          <Col sm="6">
            <Button variant="black" className="m-3">
              <Link className="navbar-link" to={`/books/${false}`}>Explore Books</Link>
            </Button>
          </Col>
        </Row>
        <p className="login-register-footer">Can't find your book here? Click here to <Link className="general-link" to="/request-book">Request A Book!</Link></p>
      </div>
      <Modal show={showRenew} onHide={handleRenewClose}>
        <Modal.Header closeButton>
          <Modal.Title>{renewTitle}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {renewBody}
        </Modal.Body>
        <Modal.Footer>
          <Button variant="black" onClick={renewBook}>
            Yes
          </Button>
          <Button variant="black" onClick={handleRenewClose}>
            No
          </Button>
        </Modal.Footer>
      </Modal>
      <Modal show={showReturn} onHide={handleReturnClose}>
        <Modal.Header closeButton>
          <Modal.Title>{returnTitle}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {returnBody}
        </Modal.Body>
        <Modal.Footer>
          <Button variant="black" onClick={returnBook}>
            Yes
          </Button>
          <Button variant="black" onClick={handleReturnClose}>
            No
          </Button>
        </Modal.Footer>
      </Modal>
      <Modal show={showNotification} onHide={handleNotificationClose}>
        <Modal.Header closeButton>
          <Modal.Title>{notificationTitle}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {notificationBody}
        </Modal.Body>
        <Modal.Footer>
          <Button variant="black" onClick={handleNotificationClose}>
            Okay
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
};

export default UserDashboardPage;
