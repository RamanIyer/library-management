import React, { useState } from 'react';
import { Button, Modal } from 'react-bootstrap';
import { useHistory } from 'react-router-dom';
import axios from 'axios';

const AddAdminPage = () => {
  const history = useHistory();
  const [errors, setErrors] = useState({});
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [password2, setPassword2] = useState("");
  const [show, setShow] = useState(false);

  const addAdmin = () => {
    const newAdmin = {};
    newAdmin.name = name;
    newAdmin.email = email;
    newAdmin.password = password;
    newAdmin.password2 = password2;

    axios.post("/api/users/addAdmin", newAdmin)
      .then(res => setShow(true))
      .catch(err => setErrors(err.response.data));
  };

  const handleClose = () => {
    setShow(false);
    history.push("/super-admin/dashboard");
  };

  return (
    <>
      <div className="add-admin-page">
        <h1 className="page-heading mt-3 mb-4">Add Admin</h1>
        <input className="input-field" placeholder="Name" value={name} onChange={(e) => setName(e.target.value)} />
        <div className="errors">
          {errors.name}
        </div>
        <input className="input-field" placeholder="Email" value={email} onChange={(e) => setEmail(e.target.value)} />
        <div className="errors">
          {errors.email}
        </div>
        <input className="input-field" placeholder="Password" value={password} type="password" onChange={(e) => setPassword(e.target.value)} />
        <div className="errors">
          {errors.password}
        </div>
        <input className="input-field" placeholder="Re-Enter Password" value={password2} type="password" onChange={(e) => setPassword2(e.target.value)} />
        <div className="errors">
          {errors.password2}
        </div>
        <Button variant="black" onClick={addAdmin}>Add Admin</Button>
      </div>
      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Added Successfully</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <p>Admin has been added successfully!</p>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="black" onClick={handleClose}>
            Close
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
};

export default AddAdminPage;
