import React, { useState, useEffect } from 'react';
import { Button, Form } from 'react-bootstrap';
import { Link, useHistory } from 'react-router-dom';
import axios from 'axios';

const AddBranchPage = () => {
  const history = useHistory();
  const [errors, setErrors] = useState({});
  const [name, setName] = useState("");
  const [location, setLocation] = useState("");
  const [admin, setAdmin] = useState("");
  const [phone, setPhone] = useState("");
  const [email, setEmail] = useState("");
  const [admins, setAdmins] = useState([]);

  useEffect(() => {
    axios.get("/api/users/getAvailableAdmins")
      .then(res => setAdmins(res.data));
  }, []);

  const setAdminDetails = ((e) => {
    const adminId = e.target.value;
    axios.get(`/api/users/getUserById/${adminId}`)
      .then(res => {
        setEmail(res.data.email);
        setAdmin(adminId);
      });
  });

  const addBranch = () => {
    const newBranch = {};
    newBranch.name = name;
    newBranch.location = location;
    newBranch.admin = admin;
    newBranch.phone = phone;
    newBranch.email = email;

    axios.post("/api/branches/addBranch", newBranch)
      .then(res => history.push("/super-admin/dashboard"))
      .catch(err => setErrors(err.response.data));
  };
  if (admins.length) {
    return (
      <div className="add-branch-page">
        <h1 className="page-heading mt-3 mb-4">Add Branch</h1>
        <input className="input-field" placeholder="Branch Name" value={name} onChange={(e) => setName(e.target.value)} />
        <div className="errors">
          {errors.name}
        </div>
        <input className="input-field" placeholder="Branch Location" value={location} onChange={(e) => setLocation(e.target.value)} />
        <div className="errors">
          {errors.location}
        </div>
        <Form.Control as="select" className="input-field" onChange={setAdminDetails}>
          <option disabled selected>Admin</option>
          {
            admins.map((admin, index) => {
              return (
                <option key={index} value={admin._id}>{admin.name}</option>
              );
            })
          }
        </Form.Control>
        <div className="errors">
          {errors.admin}
        </div>
        <input className="input-field" placeholder="Branch Phone" value={phone} onChange={(e) => setPhone(e.target.value)} />
        <div className="errors">
          {errors.phone}
        </div>
        <input className="input-field" placeholder="E-mail" value={email} disabled />
        <div className="errors">
          {errors.email}
        </div>
        <Button variant="black" onClick={addBranch}>Add Branch</Button>
      </div>
    );
  }
  else {
    return (
      <div className="add-branch-page">
        <h1 className="page-heading mt-3 mb-4">Add Branch</h1>
        <h3>There are no available admins to assign to this branch!</h3>
        <h3>Please Create an admin and then add a branch!</h3>
        <p className="login-register-footer">Click here to <Link className="general-link" to="/admin/add">Add Admin</Link></p>
      </div>
    );
  }
};

export default AddBranchPage;
