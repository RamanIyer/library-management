import axios from 'axios';
import React, { useState } from 'react';
import { Button, Modal } from 'react-bootstrap';
import { useHistory } from 'react-router-dom';

const RequestBookPage = () => {
  const history = useHistory();
  const [errors, setErrors] = useState({});
  const [name, setName] = useState("");
  const [author, setAuthor] = useState("");
  const [isbn, setIsbn] = useState("");
  const [genre, setGenre] = useState("");
  const [show, setShow] = useState(false);

  const { branch: branchId } = JSON.parse(localStorage.getItem("currentUser"));

  const handleClose = () => {
    setShow(false);
    setName("");
    setAuthor("");
    setIsbn("");
    setGenre("");
  };

  const requestBook = () => {
    const newRequest = {};
    newRequest.name = name;
    newRequest.author = author;
    newRequest.isbn = isbn;
    newRequest.genre = genre;
    newRequest.branch = branchId;

    axios.post("/api/books/requestBook", newRequest)
      .then(res => {
        setShow(true);
      })
      .catch(err => setErrors(err.response.data));
  };

  return(
    <>
      <div className="add-books-page">
        <h1 className="page-heading mt-3 mb-4">Add Books</h1>
        <input className="input-field" placeholder="Book Name" value={name} onChange={(e) => setName(e.target.value)} />
        <div className="errors">
          {errors.name}
        </div>
        <input className="input-field" placeholder="Author" value={author} onChange={(e) => setAuthor(e.target.value)} />
        <div className="errors">
          {errors.author}
        </div>
        <input className="input-field" placeholder="ISBN" value={isbn} onChange={(e) => setIsbn(e.target.value)} />
        <div className="errors">
          {errors.isbn}
        </div>
        <input className="input-field" placeholder="Genre" value={genre} onChange={(e) => setGenre(e.target.value)} />
        <div className="errors">
          {errors.genre}
        </div>
        <p><Button variant="black" onClick={requestBook}>Request Book</Button></p>
        <p><Button variant="black" onClick={() => {history.push("/user/dashboard")}}>Back</Button></p>
      </div>
      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Book Requested Successfully</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <p>The book has been requested successfully!</p>
          <p>It will be added to the branch soon!</p>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="black" onClick={handleClose}>
            Close
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
};

export default RequestBookPage;
