import React, { useState, useEffect } from 'react';
import { Button, Form, Modal } from 'react-bootstrap';
import { Link, useHistory } from 'react-router-dom';
import axios from 'axios';

const RegisterPage = () => {
  const history = useHistory();
  const [errors, setErrors] = useState({});
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [branch, setBranch] = useState("");
  const [branches, setBranches] = useState([]);
  const [show, setShow] = useState(false);
  const [registrationId, setRegistrationId] = useState("");

  useEffect(() => {
    axios.get("api/branches/getAll")
      .then(res => {
        const branchData = res.data.map((branch) => {
          return {
            value: branch._id,
            label: branch.name,
          }
        });
        setBranches(branchData);
      });
  }, []);
  
  const handleClose = () => {
    setShow(false);
    history.push("/registration/status");
  };

  const registerUser = () => {
    const newUser = {};
    newUser.name = name;
    newUser.email = email;
    newUser.branch = branch;

    axios.post("/api/registrations/register", newUser)
      .then(res => {
        setRegistrationId(res.data._id);
        setShow(true);
      })
      .catch(err => setErrors(err.response.data));
  };

  return (
    <>
      <div className="register-page">
        <h1 className="page-heading mt-3 mb-4">Register</h1>
        <input className="input-field" placeholder="User Name" value={name} onChange={(e) => setName(e.target.value)} />
        <div className="errors">
          {errors.name}
        </div>
        <input className="input-field" placeholder="E-Mail ID" value={email} onChange={(e) => setEmail(e.target.value)} />
        <div className="errors">
          {errors.email}
        </div>
        <Form.Control as="select" className="input-field" onChange={(e) => setBranch(e.target.value)}>
          <option disabled selected>Branch</option>
          {
            branches.map((branch, index) => {
              return (
                <option key={index} value={branch.value}>{branch.label}</option>
              );
            })
          }
        </Form.Control>
        <div className="errors">  
          {errors.branch}
        </div>
        <Button variant="black" onClick={registerUser}>APPLY FOR REGISTRATION</Button>
        <p className="login-register-footer">Already applied for registration? Click here to <Link className="general-link" to="/registration/status">View Status!</Link></p>
        <p className="login-register-footer">Already have an account? Click here to <Link className="general-link" to="/login">Login!</Link></p>
      </div>
      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Registered Successfully</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <p>Please note down your registration ID to track your registration status</p>
          <p>You cannot get your registration ID after you close this modal</p>
          <p>Copy this and keep it safe!</p>
          <p><b>Registration Id:- {registrationId}</b></p>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="black" onClick={handleClose}>
            Close
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
}

export default RegisterPage;
