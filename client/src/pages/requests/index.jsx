import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { Button, Col, Modal, Row } from 'react-bootstrap';
import { useHistory } from 'react-router-dom';
import ListingCard from '../../components/common/ListingCard';

const RequestsPage = () => {
  const history = useHistory();
  const [requests, setRequests] = useState([]);
  const [show, setShow] = useState(false);
  const [modalTitle, setModalTitle] = useState("");
  const [modalBody, setModalBody] = useState("");

  const { _id:adminId, isSuperAdmin } = JSON.parse(localStorage.getItem("currentUser"));
  const superAdminUser = isSuperAdmin ? true : false;
  useEffect(() => {
    axios.get(`/api/branches/getBranchByAdminId/${adminId}`)
      .then(res => {
        const branchId = res.data._id;
        axios.get(`/api/books/getAllRequests/${branchId}/${superAdminUser}`)
          .then(res => {
            setRequests(res.data);
          });
      });
  }, [adminId, superAdminUser]);

  const handleClose = () => {
    setShow(false);
    if (superAdminUser) {
      history.push('/books/add');
    }
    axios.get(`/api/branches/getBranchByAdminId/${adminId}`)
      .then(res => {
        const branchId = res.data._id;
        axios.get(`/api/books/getAllRequests/${branchId}/${superAdminUser}`)
          .then(res => {
            setRequests(res.data);
          });
      });
  };

  const approveRequest = (requestId) => {
    axios.post(`/api/books/fulfillRequest`, { requestId })
      .then(res => {
        setModalTitle("Request Approved");
        if (superAdminUser) {
          setModalBody("Request has been approved successfully! Add the book to the branch");
        }
        else {
          setModalBody("Request has been approved successfully!");
        }
        setShow(true);
      });
  };

  const rejectRequest = (requestId) => {
    axios.post(`/api/books/rejectRequest`, { requestId })
      .then(res => {
        setModalTitle("Registration Rejected");
        setModalBody("Registration rejected successfully!");
        setShow(true);
      });
  };

  return (
    <>
      <div className="request-page">
        <h1 className="page-heading mt-3 mb-4">Requests</h1>
        {
          requests.map((request, index) => {
            return (
              <Row key={index} className="mb-2">
                <Col sm="12">
                  <ListingCard
                    header={request.name}
                    details={{ author: request.author, isbn: request.isbn, genre: request.genre }}
                    listingId={request._id}
                    action1={approveRequest}
                    action2={rejectRequest}
                    action1Text="Approve"
                    action2Text="Reject"
                  />
                </Col>
              </Row>
            )
          })
        }
      </div>
      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>{modalTitle}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <p>{modalBody}</p>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="black" onClick={handleClose}>
            Close
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
};

export default RequestsPage;
