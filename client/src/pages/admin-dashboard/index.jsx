import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { Button, Col, Row } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import StatsCard from '../../components/common/StatsCard';

const AdminDashboardPage = () => {
  const [userCount, setUserCount] = useState(0);
  const [bookCount, setBookCount] = useState(0);
  const [registrationCount, setRegistrationCount] = useState(0);
  const [requestCount, setRequestCount] = useState(0);

  useEffect(() => {
    const { _id: adminId } = JSON.parse(localStorage.getItem("currentUser"));
    axios.get(`/api/branches/getBranchByAdminId/${adminId}`)
      .then(res => {
        const branchId = res.data._id;
        axios.get(`/api/statistics/userCount/${branchId}`)
          .then(res => setUserCount(res.data));
        
        axios.get(`/api/statistics/bookCount/${branchId}`)
          .then(res => setBookCount(res.data));
        
        axios.get(`/api/statistics/registrationCount/${branchId}`)
          .then(res => setRegistrationCount(res.data));
        
        axios.get(`/api/statistics/requestCount/${branchId}`)
          .then(res => setRequestCount(res.data));
      });
  }, []);

  return (
    <div className="dashboard-page">
      <h1 className="page-heading mt-3 mb-4">Dashboard</h1>
      <h3 className="mb-5">Registration Details</h3>
      <Row className="mt-5 mb-5">
        <Col sm="6">
          <StatsCard count={registrationCount} entity="Registrations" message="Pending to be approved." />
        </Col>
        <Col sm="6">
          <Button variant="black" className="m-3">
            <Link className="navbar-link" to="/registrations">View Registrations</Link>
          </Button>
        </Col>
      </Row>
      <h3 className="mb-5">Book Request Details</h3>
      <Row className="mt-5 mb-5">
        <Col sm="6">
          <StatsCard count={requestCount} entity="Book Requests" message="Check it Out Soon!." />
        </Col>
        <Col sm="6">
          <Button variant="black" className="m-3">
            <Link className="navbar-link" to="/requests">View Requests</Link>
          </Button>
        </Col>
      </Row>
      <h3 className="mb-5">Branch Details</h3>
      <Row className="mt-5 mb-5">
        <Col sm="4">
          <StatsCard count={bookCount} entity="Books" message="Currently In The Branch Library." />
        </Col>
        <Col sm="4">
          <StatsCard count={userCount} entity="Users" message="Registered to the branch." />
        </Col>
        <Col sm="4">
          <Button variant="black" className="m-3">
            <Link className="navbar-link" to="/users">View Users</Link>
          </Button>
        </Col>
      </Row>
    </div>
  );
};

export default AdminDashboardPage;
