import React, { useEffect, useState } from 'react';
import { Button, Col, Form, Modal, Row } from 'react-bootstrap';
import { Link, useHistory, useParams } from 'react-router-dom';
import axios from 'axios';
import StatsCard from '../../components/common/StatsCard';

const BranchDetailsPage = () => {
  const { branch_id: branchId } = useParams();
  const history = useHistory();
  const [name, setName] = useState("");
  const [location, setLocation] = useState("");
  const [admin, setAdmin] = useState("");
  const [newAdmin, setNewAdmin] = useState("");
  const [adminName, setAdminName] = useState("");
  const [phone, setPhone] = useState("");
  const [email, setEmail] = useState("");
  const [show, setShow] = useState(false);
  const [isChangeAdmin, setIsChangeAdmin] = useState(false);
  const [admins, setAdmins] = useState([]);
  const [userCount, setUserCount] = useState(0);
  const [bookCount, setBookCount] = useState(0);

  useEffect(() => {
    axios.get(`/api/branches/branch/${branchId}`)
      .then(res => {
        const { name, location, admin, phone, email } = res.data;
        axios.get(`/api/users/getUserById/${admin}`)
          .then(res => {
            setName(name);
            setLocation(location);
            setAdmin(admin);
            setAdminName(res.data.name);
            setPhone(phone);
            setEmail(email);
          });
      });
    
    axios.get("/api/users/getAvailableAdmins")
      .then(res => setAdmins(res.data));
    
    axios.get(`/api/statistics/userCount/${branchId}`)
      .then(res => setUserCount(res.data));
    
    axios.get(`/api/statistics/bookCount/${branchId}`)
      .then(res => setBookCount(res.data));
  }, [branchId]);

  const deleteBranch = () => {
    axios.delete(`/api/branches/removeBranch/${branchId}`)
      .then(res => {
        setShow(false);
        history.push("/super-admin/dashboard");
      });
  };

  const updateAdmin = () => {
    const newBranchDetails = {};
    newBranchDetails.name = name;
    newBranchDetails.location = location;
    newBranchDetails.admin = newAdmin;
    newBranchDetails.phone = phone;
    newBranchDetails.email = email;
    axios.post(`/api/branches/assignAdmin/${branchId}`, newBranchDetails)
      .then(res => {
        const admin = res.data.admin;
        axios.get(`/api/users/getUserById/${admin}`)
          .then(res => {
            setAdmin(admin);
            setAdminName(res.data.name);
            setIsChangeAdmin(false);
          });
      });
  };

  return (
    <>
      <div className="branch-details-page">
        <h1 className="page-heading mt-3 mb-4">Branch Details</h1>
        <div className="mb-4">
          <Link to="/super-admin/dashboard">
            <Button variant="black">Back To Dashboard</Button>
          </Link>
        </div>
        <h2>{name}</h2>
        <div className="mb-4">
          <Button variant="black" onClick={() => {setShow(true)}}>Remove Branch</Button>
        </div>
        <table className="branch-details-table mb-4">
          <tbody>
            <tr>
              <td colSpan="4" className="heading-row">
                <h3>Branch Details</h3>
              </td>
            </tr>
            <tr>
              <td className="key">Branch Name</td>
              <td className="separator">:</td>
              <td className="spacer"></td>
              <td className="value">{name}</td>
            </tr>
            <tr>
              <td className="key">Branch Location</td>
              <td className="separator">:</td>
              <td className="spacer"></td>
              <td className="value">{location}</td>
            </tr>
            {
              isChangeAdmin ? (
                <tr>
                  <td className="key">Branch Admin</td>
                  <td className="separator">:</td>
                  <td className="spacer"></td>
                  <td className="value">
                    <Form.Control as="select" className="input-field" onChange={(e) => setNewAdmin(e.target.value)}>
                      <option value={admin}>{adminName}</option>
                      {
                        admins.map((admin, index) => {
                          return (
                            <option key={index} value={admin._id}>{admin.name}</option>
                          );
                        })
                      }
                    </Form.Control>
                    <Button variant="form-black" onClick={updateAdmin}>Update Admin</Button>
                    <Button variant="form-black" onClick={() => setIsChangeAdmin(false)}>Cancel</Button>
                  </td>
                </tr>
              ): (
                <tr>
                  <td className="key">Branch Admin</td>
                  <td className="separator">:</td>
                  <td className="spacer"></td>
                  <td className="value">
                    {adminName}
                    <Button variant="form-black" onClick={() => setIsChangeAdmin(true)}>Change Admin</Button>
                  </td>
                </tr>
            )}
            <tr>
              <td className="key">Branch Phone</td>
              <td className="separator">:</td>
              <td className="spacer"></td>
              <td className="value">{phone}</td>
            </tr>
            <tr>
              <td className="key">Branch Email</td>
              <td className="separator">:</td>
              <td className="spacer"></td>
              <td className="value">{email}</td>
            </tr>
            <tr>
              <td colSpan="4" className="heading-row">
                <h3>Statistics</h3>
              </td>
            </tr>
          </tbody>
        </table>
        <Row className="mb-5">
          <Col sm="6">
            <StatsCard count={userCount} entity="users" message="Have registered with the branch." />
          </Col>
          <Col sm="6">
            <StatsCard count={bookCount} entity="books" message="In the branch's library." />
          </Col>
        </Row>
      </div>
      <Modal show={show} onHide={() => { setShow(false) }}>
        <Modal.Header closeButton>
          <Modal.Title>Remove Branch</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          Are You Sure You Want To Remove The Branch?
        </Modal.Body>
        <Modal.Footer>
          <Button variant="black" onClick={deleteBranch}>Yes</Button>
          <Button variant="black" onClick={() => setShow(false)}>No</Button>
        </Modal.Footer>
      </Modal>
    </>
  );
}

export default BranchDetailsPage;
