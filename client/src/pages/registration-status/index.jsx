import React, { useState } from 'react';
import { Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import axios from 'axios';
import StatusIndicator from '../../components/registration-status/StatusIndicator';

const RegistrationStatusPage = () => {
  const [errors, setErrors] = useState({});
  const [registrationId, setRegistrationId] = useState("");
  const [status, setStatus] = useState();

  const getStatus = () => {
    axios.post(`/api/registrations/getRegistration`, { registrationId })
      .then(res => {
        setStatus(res.data);
      })
      .catch(err => setErrors(err.response.data));
  };

  return (
    <div className="register-page">
      <h1 className="page-heading mt-3 mb-4">Registration Status</h1>
      <input className="input-field" placeholder="Registration Id" value={registrationId} onChange={(e) => setRegistrationId(e.target.value)} />
      <div className="errors">
        {errors.registrationId}
      </div>
      <Button className="mb-4" variant="black" onClick={getStatus}>GET STATUS</Button>
      {status &&
        <StatusIndicator status={status} />
      }
      <p className="login-register-footer">Back to <Link className="general-link" to="/login">Login!</Link></p>
    </div>
  );
};

export default RegistrationStatusPage;
