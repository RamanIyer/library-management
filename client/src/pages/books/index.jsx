import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { Button, Col, Modal, Row } from 'react-bootstrap';
import { useHistory, useParams } from 'react-router-dom';
import ListingCard from '../../components/common/ListingCard';

const BooksPage = () => {
  const history = useHistory();
  const { is_issue_option: isIssuePage } = useParams();
  const [books, setBooks] = useState([]);
  const [show, setShow] = useState(false);
  const [modalTitle, setModalTitle] = useState("");
  const [modalBody, setModalBody] = useState("");

  const { branch: branchId } = JSON.parse(localStorage.getItem("currentUser"));

  
  useEffect(() => {
    axios.get(`/api/books/getAll/${branchId}`)
      .then(res => {
        setBooks(res.data);
      });
  }, [branchId]);

  const handleClose = () => {
    setShow(false);
    history.push('/user/dashboard');
  };

  const issueBook = (bookId) => {
    let newIssue = {
      bookId
    }

    axios.post("/api/issues/issueBook", newIssue)
      .then(() => {
        setModalTitle("Book Issue");
        setModalBody("Book issued successfully!");
        setShow(true);
      });
  };

  return (
    <>
      <div className="books-page">
        {isIssuePage === "true" ? (
          <h1 className="page-heading mt-3 mb-4">Issue Book</h1>
        ): (
          <h1 className="page-heading mt-3 mb-4">Books</h1>
        )
        }
        {
          books.map((book, index) => {
            return (
              <Row key={index} className="mb-2">
                <Col sm="12">
                  {isIssuePage === "true" ? (
                    <ListingCard
                      header={book.name}
                      details={{ author: book.author, isbn: book.isbn, genre: book.genre }}
                      listingId={book._id}
                      action1={issueBook}
                      action1Text="Issue"
                    />
                  ): (
                    <ListingCard
                      header={book.name}
                      details={{ author: book.author, isbn: book.isbn, genre: book.genre }}
                      listingId={book._id}
                    />
                  )
                  }
                </Col>
              </Row>
            )
          })
        }
      </div>
      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>{modalTitle}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <p>{modalBody}</p>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="black" onClick={handleClose}>
            Close
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
};

export default BooksPage;
