import React from 'react';
import { Link } from 'react-router-dom';

const PageNotFound = () => {
  return (
    <div className="home-page">
      <h1 className="page-heading mt-3 mb-4">Page Not Found</h1>
      <p className="login-register-footer">Go Back <Link className="general-link" to="/">Home!</Link></p>
    </div>
  );
};

export default PageNotFound;
