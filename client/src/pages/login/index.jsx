import React, { useState } from 'react';
import { Button } from 'react-bootstrap';
import { Link, useHistory } from 'react-router-dom';
import axios from 'axios';

const LoginPage = ({setToken}) => {
  const history = useHistory();
  const [errors, setErrors] = useState({});
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const loginUser = () => {
    const userLogin = {};
    userLogin.email = email;
    userLogin.password = password;

    axios.post("/api/users/login", userLogin)
      .then(res => {
        const { token, isAdmin, isSuperAdmin } = res.data;
        setToken(token, isAdmin, isSuperAdmin);
        axios.get("/api/users/current")
          .then(res => {
            localStorage.setItem("currentUser", JSON.stringify(res.data));
            if (isAdmin) {
              history.push("/admin/dashboard");
            } else if (isSuperAdmin) {
              history.push("/super-admin/dashboard");
            } else {
              history.push("/user/dashboard");
            }
          });
      })
      .catch(err => setErrors(err.response.data));
  };

  return(
    <div className="login-page">
      <h1 className="page-heading mt-3 mb-4">Login</h1>
      <input className="input-field" placeholder="E-Mail ID" value={email} onChange={(e) => setEmail(e.target.value)} />
      <div className="errors">
        {errors.email}
      </div>
      <input type="password" className="input-field" placeholder="Password" value={password} onChange={(e) => setPassword(e.target.value)} />
      <div className="errors">
        {errors.password}
      </div>
      <Button variant="black" onClick={loginUser}>LOGIN</Button>
      <p className="login-register-footer">Not a user yet? Click here to <Link className="general-link" to="/register">Register!</Link></p>
      <p className="login-register-footer">Already applied for registration? Click here to <Link className="general-link" to="/registration/status">View Status!</Link></p>
    </div>
  );
}

export default LoginPage;
