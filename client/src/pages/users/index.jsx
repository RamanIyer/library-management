import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { Button, Col, Row } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import ListingCard from '../../components/common/ListingCard';

const UsersPage = () => {
  const [users, setUsers] = useState([]);

  const { _id:adminId, isSuperAdmin } = JSON.parse(localStorage.getItem("currentUser"));

  useEffect(() => {
    axios.get(`/api/branches/getBranchByAdminId/${adminId}`)
      .then(res => {
        const branchId = res.data._id;
        axios.get(`/api/users/getUsersByBranch/${branchId}`)
          .then(res => {
            setUsers(res.data);
          });
      });
  }, [adminId, isSuperAdmin]);

  const removeUser = (userId) => {
    axios.delete(`/api/users/removeUser/${userId}`);
  };

  return (
    <div className="request-page">
      <h1 className="page-heading mt-3 mb-4">Users</h1>
      {
        users.map((user, index) => {
          return (
            <Row key={index} className="mb-2">
              <Col sm="12">
                <ListingCard
                  header={user.name}
                  details={{ email: user.email }}
                  listingId={user._id}
                  action1={removeUser}
                  action1Text="Remove"
                />
              </Col>
            </Row>
          )
        })
      }
      <Link to="/admin/dashboard">
        <Button variant="black">Back</Button>
      </Link>
    </div>
  );
};

export default UsersPage;
