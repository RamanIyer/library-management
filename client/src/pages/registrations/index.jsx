import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { Button, Col, Modal, Row } from 'react-bootstrap';
import ListingCard from '../../components/common/ListingCard';

const RegistrationsPage = () => {
  const [registrations, setRegistrations] = useState([]);
  const [show, setShow] = useState(false);
  const [modalTitle, setModalTitle] = useState("");
  const [modalBody, setModalBody] = useState("");

  const handleClose = () => {
    setShow(false);
    const { _id: adminId } = JSON.parse(localStorage.getItem("currentUser"));
    axios.get(`/api/branches/getBranchByAdminId/${adminId}`)
      .then(res => {
        const branchId = res.data._id;
        axios.get(`/api/registrations/getRegistration/${branchId}`)
          .then(res => {
            const pendingRegistrations = res.data;
            setRegistrations(res.data);
            pendingRegistrations.forEach(registration => {
              const newRegistration = registration;
              newRegistration.status = "Application Viewed"
              axios.post(`/api/registrations/updateStatus/${registration._id}`, newRegistration);
            });
          });
      });
  };

  useEffect(() => {
    const { _id: adminId } = JSON.parse(localStorage.getItem("currentUser"));
    axios.get(`/api/branches/getBranchByAdminId/${adminId}`)
      .then(res => {
        const branchId = res.data._id;
        axios.get(`/api/registrations/getRegistration/${branchId}`)
          .then(res => {
            const pendingRegistrations = res.data;
            setRegistrations(res.data);
            pendingRegistrations.forEach(registration => {
              const newRegistration = registration;
              newRegistration.status = "Application Viewed"
              axios.post(`/api/registrations/updateStatus/${registration._id}`, newRegistration);
            });
          });
      });
  }, []);

  const approveRegistration = (registrationId) => {
    const { _id: adminId } = JSON.parse(localStorage.getItem("currentUser"));
    axios.get(`/api/branches/getBranchByAdminId/${adminId}`)
      .then(res => {
        const branchId = res.data._id;
        axios.post(`/api/registrations/getRegistration`, { registrationId })
          .then(res => {
            const registration = res.data;
            const newUser = {};
            newUser.name = registration.name;
            newUser.email = registration.email;
            const password = generatePassword(registration.name);
            newUser.password = password;
            newUser.password2 = password;
            newUser.branch = branchId;
            axios.post("/api/users/addUser", newUser)
              .then(res => {
                axios.post(`/api/registrations/getRegistration`, { registrationId })
                  .then(res => {
                    const newRegistration = res.data;
                    newRegistration.status = "Application Approved";
                    newRegistration.isUserCreated = true;
                    axios.post(`/api/registrations/updateStatus/${registrationId}`, newRegistration)
                      .then(res => {
                        setModalTitle("Added Successfully");
                        setModalBody("User has been added successfully!");
                        setShow(true);
                      });
                  });
              });
          });
      });
  };

  const generatePassword = (name) => {
    let password = name.toLowerCase();
    password = password.split(" ").join("_");
    password += "@123";
    return password;
  };

  const rejectRegistration = (registrationId) => {
    axios.post(`/api/registrations/getRegistration`, { registrationId })
      .then(res => {
        const newRegistration = res.data;
        newRegistration.status = "Application Rejected"
        axios.post(`/api/registrations/updateStatus/${registrationId}`, newRegistration)
          .then(res => {
            setModalTitle("Registration Rejected");
            setModalBody("Registration rejected successfully!");
            setShow(true);
          });
      });
  };

  return (
    <>
      <div className="registration-page">
        <h1 className="page-heading mt-3 mb-4">Registrations</h1>
        {
          registrations.map((registration, index) => {
            return (
              <Row key={index} className="mb-2">
                <Col sm="12">
                  <ListingCard
                    header={registration.name}
                    details={{ email: registration.email }}
                    listingId={registration._id}
                    action1={approveRegistration}
                    action2={rejectRegistration}
                    action1Text="Approve"
                    action2Text="Reject"
                  />
                </Col>
              </Row>
            )
          })
        }
      </div>
      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>{modalTitle}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <p>{modalBody}</p>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="black" onClick={handleClose}>
            Close
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
};

export default RegistrationsPage;
