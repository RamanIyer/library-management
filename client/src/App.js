import { BrowserRouter as Router } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import AppRouter from './AppRouter';
import NavHeader from './components/common/NavHeader';
import { useEffect, useState } from 'react';
import setAxiosAuth from './utils/setAxiosAuth';
import '@fortawesome/fontawesome-free/css/all.css';

function App() {
  const [authToken, setAuthToken] = useState('');
  const [isAdmin, setIsAdmin] = useState(false);
  const [isSuperAdmin, setIsSuperAdmin] = useState(false);

  useEffect(() => {
    const token = localStorage.getItem("token");
    if (token && token !== '') {
      setAuthToken(token);
      setAxiosAuth(token);
    }
    const adminUser = localStorage.getItem("isAdmin");
    if (adminUser === "true") {
      setIsAdmin(adminUser);
    }
    const superAdminUser = localStorage.getItem("isSuperAdmin");
    if (superAdminUser === "true") {
      setIsSuperAdmin(superAdminUser);
    }
  }, [])

  const setToken = (token, isAdmin, isSuperAdmin) => {
    if (token === '') {
      localStorage.removeItem("token");
      localStorage.removeItem("isAdmin");
      localStorage.removeItem("isSuperAdmin");
      setAuthToken('');
      setAxiosAuth('');
      setIsAdmin(false);
      setIsSuperAdmin(false);
    }
    else{
      localStorage.setItem("token", token);
      localStorage.setItem("isAdmin", isAdmin);
      localStorage.setItem("isSuperAdmin", isSuperAdmin);
      setAuthToken(token);
      setAxiosAuth(token);
      setIsAdmin(isAdmin);
      setIsSuperAdmin(isSuperAdmin);
    }
  };

  return (
    <div className="app">
      <Router>
        <NavHeader isLogin={authToken !== ''} setToken={setToken} />
        <div className="content-wrapper">
          <AppRouter isLogin={authToken !== ''} isAdmin={isAdmin} isSuperAdmin={isSuperAdmin} setToken={setToken} />
        </div>
      </Router>
    </div>
  );
}

export default App;
