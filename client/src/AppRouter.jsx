import React from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import { ROUTES } from './utils/constants';

import PageNotFound from './pages/page-not-found';
import HomePage from './pages/home';
import LoginPage from './pages/login';
import RegisterPage from './pages/register';
import RegistrationStatusPage from './pages/registration-status';
import UserDashboardPage from './pages/user-dashboard';
import BooksPage from './pages/books';
import RequestBookPage from './pages/request-book';
import AdminDashboardPage from './pages/admin-dashboard';
import UsersPage from './pages/users';
import SuperAdminDashboardPage from './pages/super-admin-dashboard';
import AddBranchPage from './pages/add-branch';
import BranchDetailsPage from './pages/branch-details';
import AddAdminPage from './pages/add-admin';
import AddBooksPage from './pages/add-books';
import RegistrationsPage from './pages/registrations';
import RequestsPage from './pages/requests';

const AppRouter = ({ isLogin, isAdmin, isSuperAdmin, setToken }) => {
  return (
    <Switch>
      {/* Common Routes */}
      <Route exact path={ROUTES.NOT_FOUND} component={PageNotFound} />
      <Route exact path={ROUTES.HOME} component={HomePage}>
        {
          isLogin && isSuperAdmin ?
            <Redirect to={ROUTES.DASHBOARD_SUPER_ADMIN} />
            : isLogin && isAdmin ?
              <Redirect to={ROUTES.DASHBOARD_ADMIN} />
              : isLogin ?
                <Redirect to={ROUTES.DASHBOARD_USER} />
                : <HomePage />
        }
      </Route>
      <Route exact path={ROUTES.LOGIN} render={() => <LoginPage setToken={setToken} />}>
        {isLogin && <Redirect to={ROUTES.HOME} />}
      </Route>
      <Route exact path={ROUTES.REGISTER} component={RegisterPage}>
        {isLogin && <Redirect to={ROUTES.HOME} />}
      </Route>
      <Route exact path={ROUTES.REGISTRATION_STATUS} component={RegistrationStatusPage}>
        {isLogin && <Redirect to={ROUTES.HOME} />}
      </Route>
      {/* User Routes */}
      <Route exact path={ROUTES.DASHBOARD_USER} component={UserDashboardPage}>
        {!isLogin && <Redirect to={ROUTES.HOME} />}
      </Route>
      <Route exact path={ROUTES.BOOKS} component={BooksPage}>
        {!isLogin && <Redirect to={ROUTES.HOME} />}
      </Route>
      <Route exact path={ROUTES.REQUEST_BOOK} component={RequestBookPage}>
        {!isLogin && <Redirect to={ROUTES.HOME} />}
      </Route>
      {/* Admin Routes */}
      <Route exact path={ROUTES.DASHBOARD_ADMIN} component={AdminDashboardPage}>
        {!isLogin && <Redirect to={ROUTES.HOME} />}
      </Route>
      <Route exact path={ROUTES.USERS} component={UsersPage}>
        {!isLogin && <Redirect to={ROUTES.HOME} />}
      </Route>
      <Route exact path={ROUTES.REGISTRATIONS} component={RegistrationsPage}>
        {!isLogin && <Redirect to={ROUTES.HOME} />}
      </Route>
      {/* Super Admin Routes */}
      <Route exact path={ROUTES.DASHBOARD_SUPER_ADMIN} component={SuperAdminDashboardPage}>
        {!isLogin && <Redirect to={ROUTES.HOME} />}
      </Route>
      <Route exact path={ROUTES.BRANCH_ADD} component={AddBranchPage}>
        {!isLogin && <Redirect to={ROUTES.HOME} />}
      </Route>
      <Route exact path={ROUTES.BRANCH_DETAILS} component={BranchDetailsPage}>
        {!isLogin && <Redirect to={ROUTES.HOME} />}
      </Route>
      <Route exact path={ROUTES.ADMIN_ADD} component={AddAdminPage}>
        {!isLogin && <Redirect to={ROUTES.HOME} />}
      </Route>
      <Route exact path={ROUTES.BOOKS_ADD} component={AddBooksPage}>
        {!isLogin && <Redirect to={ROUTES.HOME} />}
      </Route>
      {/* Common Admin And Super Admin Routes */}
      <Route exact path={ROUTES.REQUESTS} component={RequestsPage}>
        {!isLogin && <Redirect to={ROUTES.HOME} />}
      </Route>
      {/* Default Route */}
      <Redirect to={ROUTES.NOT_FOUND} />
    </Switch>
  );
};

export default AppRouter;