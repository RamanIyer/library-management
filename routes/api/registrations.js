// import dependencies
const express = require("express");
const passport = require("passport");

// initialize router
const router = express.Router();

// imports for validators
const validateRegisterInput = require("../../validation/register");

// imports for database
const Registration = require("../../models/Registration");

// @route   POST api/registrations/register
// @desc    Create a new registration
// @access  Public
router.post("/register", (req, res) => {
  const { errors, isValid } = validateRegisterInput(req.body);
  if (!isValid) {
    return res.status(400).json(errors);
  }

  User.findOne({ email: req.body.email }).then((user) => {
    if (user) {
      errors.email = "E-mail already registered!";
      return res.status(400).json(errors);
    } else {
      const newUser = new Registration({
        name: req.body.name,
        email: req.body.email,
        branch: req.body.branch,
        status: "Application Sent",
        isUserCreated: false,
      });

      newUser
        .save()
        .then((user) => res.json(user))
        .catch((err) => console.log(err));
    }
  });
});

// @route   POST api/registrations/getRegistration
// @desc    Get the registration details
// @access  Public
router.post("/getRegistration", (req, res) => {
  let errors = {};
  Registration.findOne({ _id: req.body.registrationId })
    .then(registration => {
      if (!registration) {
        errors.registrationId = "Registration Id not found";
        return res.status(404).json(errors);
      }

      return res.json(registration);
    })
    .catch(() => {
      errors.registrationId = "Registration Id not found";
      return res.status(404).json(errors);
    });
});

// @route   POST api/registrations/updateStatus/:registration_id
// @desc    Update the status of the registration application
// @access  Private
router.post("/updateStatus/:registration_id", passport.authenticate("jwt", { session: false }), (req, res) => {
  Registration.findByIdAndUpdate(
    { _id: req.params.registration_id },
    { $set: req.body },
    { new: true }
  )
    .then(() => res.json("Done"))
    .catch(err => console.log(err));
});

// @route   GET api/registrations/getRegistration/:branch_id
// @desc    Get registrations for a particular branch
// @access  Private
router.get("/getRegistration/:branch_id", passport.authenticate("jwt", { session: false }), (req, res) => {
  let errors = {};
  Registration.find({ branch: req.params.branch_id, isUserCreated: false, status: {$ne : "Application Rejected"}})
    .then(registrations => {
      if (!registrations) {
        return res.status(400).json(errors);
      }

      return res.json(registrations);
    })
    .catch(() => {
      errors.noregistration = "No active registrations!"
    });
});

// export the router
module.exports = router;
