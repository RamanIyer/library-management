// import dependencies
const express = require("express");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const passport = require("passport");

// initialize router
const router = express.Router();

// imports for validators
const validateLoginInput = require("../../validation/login");
const validateUserInput = require("../../validation/addUser");

// imports for database
const User = require("../../models/User");
const Branch = require("../../models/Branch");

// @route   POST api/users/login
// @desc    Logs in the user
// @access  Public
router.post("/login", (req, res) => {
  const { errors, isValid } = validateLoginInput(req.body);
  if (!isValid) {
    return res.status(400).json(errors);
  }

  const email = req.body.email;
  const password = req.body.password;
  
  User.findOne({ email })
    .then((user) => {
      if (!user) {
        errors.email = "User not found!";
        return res.status(400).json(errors);
      }

      bcrypt.compare(password, user.password).then((isMatch) => {
        if (isMatch) {
          const payload = {
            id: user.id,
            name: user.name,
            email: user.email,
            phone: user.phone,
          }
          const isAdmin = user.isAdmin;
          const isSuperAdmin = user.isSuperAdmin;
          const secret = require('../../config/keys').SECRET_OR_KEY;
          jwt.sign(payload, secret, { expiresIn: 86400 }, (err, token) => {
            if (err) throw err;
            res.json({
              success: true,
              token: 'Bearer ' + token,
              isAdmin: isAdmin,
              isSuperAdmin: isSuperAdmin,
            })
          })
        } else {
          errors.password = "Password is incorrect!";
          res.status(400).json(errors);
        }
      });
    })
    .catch((err) => console.log(err));
});

// @route   POST api/users/addUser
// @desc    Add a new regular user
// @access  Private
router.post("/addUser", passport.authenticate("jwt", { session: false }), (req, res) => {
  const { errors, isValid } = validateUserInput(req.body);
  if (!isValid) {
    return res.status(400).json(errors);
  }

  User.findOne({ email: req.body.email }).then((user) => {
    if (user) {
      errors.email = "E-mail already registered!";
      return res.status(400).json(errors);
    } else {
      const newUser = new User({
        name: req.body.name,
        email: req.body.email,
        password: req.body.password,
        isAdmin: false,
        isSuperAdmin: false,
        branch: req.body.branch
      });

      bcrypt.genSalt(10, (err, salt) => {
        if (err) throw err;
        bcrypt.hash(newUser.password, salt, (err, hash) => {
          if (err) throw err;
          newUser.password = hash;
          newUser
            .save()
            .then((user) => res.json(user))
            .catch((err) => console.log(err));
        });
      });
    }
  });
});

// @route   POST api/users/addAdmin
// @desc    Add a new Admin user
// @access  Private
router.post("/addAdmin", passport.authenticate("jwt", { session: false }), (req, res) => {
  const { errors, isValid } = validateUserInput(req.body);
  if (!isValid) {
    return res.status(400).json(errors);
  }
  
  User.findOne({ email: req.body.email }).then((user) => {
    if (user) {
      errors.email = "E-mail already registered!";
      return res.status(400).json(errors);
    } else {
      const newUser = new User({
        name: req.body.name,
        email: req.body.email,
        password: req.body.password,
        isAdmin: true,
        isSuperAdmin: false,
      });

      bcrypt.genSalt(10, (err, salt) => {
        if (err) throw err;
        bcrypt.hash(newUser.password, salt, (err, hash) => {
          if (err) throw err;
          newUser.password = hash;
          newUser
            .save()
            .then((user) => res.json(user))
            .catch((err) => console.log(err));
        });
      });
    }
  });
});

// @route   DELETE api/users/removeUser/:user_id
// @desc    Remove a user
// @access  Private
router.delete("/removeUser/:user_id", passport.authenticate("jwt", { session: false }), (req, res) => {
  User.findByIdAndDelete({ _id: req.params.user_id })
    .then(user => res.json(user));
});

// @route   GET api/users/getUserById/:user_id
// @desc    Get a user detail
// @access  Private
router.get("/getUserById/:user_id", passport.authenticate("jwt", { session: false }), (req, res) => {
  User.findOne({ _id: req.params.user_id })
    .then(user => {
      if (!user) {
        return res.status(400).json({error: "User not found"})
      }

      return res.json(user);
    })
    .catch(err => {
      return res.status(400).json(err);
    });
});

// @route   GET api/users/getAvailableAdmins
// @desc    Get available admins not assigned to a branch
// @access  Private
router.get("/getAvailableAdmins", passport.authenticate("jwt", { session: false }), (req, res) => {
  Branch.find()
    .then(branches => {
      const admins = branches.map(branch => {
        return branch.admin.toString();
      });
      User.find({ isAdmin: true })
        .then(users => {
          let availableAdmins = [];
          users.forEach(user => {
            if (!admins.includes(user._id.toString())) {
              availableAdmins.push(user);
            }
          });

          return res.json(availableAdmins);
        })
        .catch(err => {
          return res.status(400).json(err);
        });
    })
    .catch(err => {
      return res.status(404).json(err);
    });
});

// @route   GET api/users/getUsersByBranch/:branch_id
// @desc    Get users in a branch
// @access  Private
router.get("/getUsersByBranch/:branch_id", passport.authenticate("jwt", { session: false }), (req, res) => {
  User.find({ branch: req.params.branch_id })
    .then(users => {
      if (!users) {
        return res.status(404).json({ error: "No Users In Branch!" });
      }

      return res.json(users);
    })
    .catch(err => {
      return res.status(400).json(err);
    });
});

// @route   GET api/users/current
// @desc    Get current user details
// @access  Private
router.get("/current", passport.authenticate("jwt", { session: false }), (req, res) => {
  const { _id, name, email, branch } = req.user;
  return res.json({
    _id,
    name,
    email,
    branch
  });
});

// export the router
module.exports = router;
