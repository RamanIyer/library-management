// import dependencies
const express = require("express");
const passport = require("passport");

// initialize router
const router = express.Router();

// imports for validators
const validateBranchInput = require("../../validation/addBranch");

// imports for database
const Branch = require("../../models/Branch");

// @route   GET api/branches/getAll
// @desc    Get all branches
// @access  Public
router.get("/getAll", (req, res) => {
  let errors = {};
  Branch.find()
    .then(branches => {
      if (!branches) {
        errors.nobranch = "There are no branches created";
        return res.status(404).json(errors);
      }
      
      return res.json(branches);
    })
    .catch(() => {
      errors.nobranch = "There are no branches created";
      return res.status(404).json(errors);
    });
});

// @route   GET api/branches/branch/:branch_id
// @desc    Get branch by id
// @access  Public
router.get("/branch/:branch_id", (req, res) => {
  let errors = {};
  Branch.findOne({_id: req.params.branch_id})
    .then(branch => {
      if (!branch) {
        errors.nobranch = "This branch doesn't exist!";
        return res.status(404).json(errors);
      }
      
      return res.json(branch);
    })
    .catch(() => {
      errors.nobranch = "This branch doesn't exist!";
      return res.status(404).json(errors);
    });
});

// @route   POST api/branches/addBranch
// @desc    Adds a new branch
// @access  Private
router.post("/addBranch", passport.authenticate("jwt", { session: false }), (req, res) => {
  const { errors, isValid } = validateBranchInput(req.body);
  if (!isValid) {
    return res.status(400).json(errors);
  }
  const newBranch = req.body;
  newBranch.phone = "+91" + newBranch.phone;
  new Branch(newBranch).save().then(branch => res.json(branch));
});

// @route   POST api/branches/assignAdmin/:branch_id
// @desc    Assign an admin to the branch
// @access  Private
router.post("/assignAdmin/:branch_id", passport.authenticate("jwt", { session: false }), (req, res) => {
  const errors = {};
  const newBranchDetails = req.body;

  Branch.findByIdAndUpdate(
    { _id: req.params.branch_id },
    { $set: newBranchDetails },
    { new: true }
  )
    .then(branch => res.json(branch))
    .catch(() => {
      errors.nobranch = "Branch not found";
      return res.status(404).json(errors);
    });
});

// @route   DELETE api/branches/removeBranch/:branch_id
// @desc    Remove the branch
// @access  Private
router.delete("/removeBranch/:branch_id", passport.authenticate("jwt", { session: false }), (req, res) => {
  Branch.findByIdAndDelete({ _id: req.params.branch_id })
    .then(branch => res.json(branch));
});

// @route   GET api/branches/getBranchByAdminId/:admin_id
// @desc    Get a branch by admin id
// @access  Private
router.get("/getBranchByAdminId/:admin_id", passport.authenticate("jwt", { session: false }), (req, res) => {
  let errors = {};

  Branch.findOne({ admin: req.params.admin_id })
    .then(branch => {
      if (!branch) {
        errors.nobranch = "This branch doesn't exist!";
        return res.status(404).json(errors);
      }
      
      return res.json(branch);
    })
    .catch(() => {
      errors.nobranch = "This branch doesn't exist!";
      return res.status(404).json(errors);
    });
});

//export the router
module.exports = router;
