// import dependencies
const express = require('express');
const passport = require("passport");

// initialize router
const router = express.Router();

// imports for validators
const validateBookInput = require("../../validation/book");

// imports for database
const Request = require("../../models/Request");
const Book = require("../../models/Book");

// @route   GET api/books/getAll/:branch_id
// @desc    Get all the books for the branch
// @access  Private
router.get("/getAll/:branch_id", passport.authenticate("jwt", { session: false }), (req, res) => {
  let errors = {};
  Book.find({ branch: req.params.branch_id }).then(books => {
    if (!books) {
      errors.nobook = "There are no books in the library!";
      return res.status(404).json(errors);
    }

    return res.json(books);
  })
  .catch(() => {
    errors.nobook = "There are no books in the library!";
    return res.status(404).json(errors);
  });
});

// @route   GET api/books/getBookById/:book_id
// @desc    Get a book by id
// @access  Private
router.get("/getBookById/:book_id", passport.authenticate("jwt", { session: false }), (req, res) => {
  let errors = {};

  Book.findOne({ _id: req.params.book_id }).then(book => {
    if (!book) {
      errors.nobook = "There are no books for the id!";
      return res.status(404).json(errors);
    }

    return res.json(book);
  })
  .catch(() => {
    errors.nobook = "There are no books for the id!";
    return res.status(404).json(errors);
  });
});

// @route   GET api/books/getAllRequests/:branch_id/:isSuperAdmin
// @desc    Get all the posted requests
// @access  Private
router.get("/getAllRequests/:branch_id/:isSuperAdmin", passport.authenticate("jwt", { session: false }), (req, res) => {
  let errors = {};
  const isAdminApproved = req.params.isSuperAdmin === "true" ? true : false;
  Request.find({ isRejected:false, isAdminApproved: isAdminApproved, isSuperAdminApproved: false, branch: req.params.branch_id }).then(requests => {
    if (requests) {
      return res.json(requests);
    }
    else {
      errors.norequests = "There are no requests for books!";
      return res.status(404).json(errors);
    }
  })
  .catch(() => {
    errors.norequests = "There are no requests for books!";
    return res.status(404).json(errors);
  });
});

// @route   POST api/books/requestBook
// @desc    Add a request for a new book
// @access  Private
router.post("/requestBook", passport.authenticate("jwt", { session: false }), (req, res) => {
  const { errors, isValid } = validateBookInput(req.body, true);
  if (!isValid) {
    return res.status(400).json(errors);
  }

  const { name, author, isbn, genre, branch } = req.body;
  const newRequest = new Request({
        name,
        author,
        isbn,
        genre,
        branch,
      });

      newRequest
        .save()
        .then((request) => res.json(request))
        .catch((err) => console.log(err));
});

// @route   POST api/books/fulfillRequest
// @desc    Fulfill a new book request
// @access  Private
router.post("/fulfillRequest", passport.authenticate("jwt", { session: false }), (req, res) => {
  let errors = {};
  Request.findOne({ _id: req.body.requestId }).then((request) => {
    if (request) {
      request.isSuperAdminApproved = request.isAdminApproved ? true : false;
      request.isAdminApproved = true;
      Request.findByIdAndUpdate(
        { _id: req.body.requestId },
        { $set: request },
        { new: true }
      )
      .then(() => res.json("Done"))
      .catch(err => console.log(err));
    }
    else {
      errors.norequest = "Request doesn't exist";
      return res.status(404).json(errors);
    }
  })
  .catch(() => {
    errors.norequest = "Request doesn't exist";
    return res.status(404).json(errors);
  });
});

// @route   POST api/books/rejectRequest
// @desc    Reject a new book request
// @access  Private
router.post("/rejectRequest", passport.authenticate("jwt", { session: false }), (req, res) => {
  let errors = {};
  Request.findOne({ _id: req.body.requestId }).then((request) => {
    if (request) {
      request.isRejected = true;
      Registration.findByIdAndUpdate(
        { _id: req.body.requestId },
        { $set: request },
        { new: true }
      )
      .then(() => res.json("Done"))
      .catch(err => console.log(err));
    }
    else {
      errors.norequest = "Request doesn't exist";
      return res.status(404).json(errors);
    }
  })
  .catch(() => {
    errors.norequest = "Request doesn't exist";
    return res.status(404).json(errors);
  });
});

// @route   POST api/books/addBooks
// @desc    Add a new or existing book
// @access  Private
router.post("/addBooks", passport.authenticate("jwt", { session: false }), (req, res) => {
  const { errors, isValid } = validateBookInput(req.body, false);
  if (!isValid) {
    return res.status(400).json(errors);
  }

  const { name, author, isbn, genre, quantity, branch } = req.body;

  Book.findOne({ isbn, branch }).then((book) => {
    if (book) {
      book.quantity += parseInt(quantity);
      Registration.findByIdAndUpdate(
        { _id: book._id },
        { $set: book },
        { new: true }
      )
      .then((book) => res.json(book))
      .catch(err => console.log(err));
    } else {
      const newBook = new Book({
        name,
        author,
        isbn,
        genre,
        quantity,
        branch,
      });

      newBook
        .save()
        .then((user) => res.json(user))
        .catch((err) => console.log(err));
    }
  });
});

// @route   DELETE api/books/removeBook
// @desc    Removes books from library
// @access  Private
router.post("/removeBook", passport.authenticate("jwt", { session: false }), (req, res) => {

});

// export the router
module.exports = router;
