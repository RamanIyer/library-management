// import dependencies
const express = require("express");
const passport = require("passport");

// initialize router
const router = express.Router();

// imports for database
const User = require("../../models/User");
const Branch = require("../../models/Branch");
const Book = require("../../models/Book");
const Registration = require("../../models/Registration");
const Request = require("../../models/Request");

// @route   GET api/statistics/userCount
// @desc    Returns the count of the users.
// @access  Public
router.get("/allUserCount", (req, res) => {
  User.countDocuments({}).then(count => res.json(count ? count : 0))
    .catch(err => console.log(err));
});

// @route   GET api/statistics/adminCount
// @desc    Returns the count of the admin users.
// @access  Private
router.get("/adminCount", passport.authenticate("jwt", { session: false }), (req, res) => {
  User.countDocuments({isAdmin: true}).then(count => res.json(count ? count : 0))
    .catch(err => console.log(err));
});

// @route   GET api/statistics/userCount/:branch_id
// @desc    Returns the count of the users per branch.
// @access  Private
router.get("/userCount/:branch_id", passport.authenticate("jwt", { session: false }), (req, res) => {
  User.countDocuments({branch: req.params.branch_id}).then(count => res.json(count ? count : 0))
    .catch(err => console.log(err));
});

// @route   GET api/statistics/branchCount
// @desc    Returns the count of the branches.
// @access  Public
router.get("/branchCount", (req, res) => {
  Branch.countDocuments({}).then(count => res.json(count ? count : 0))
    .catch(err => console.log(err));
});

// @route   GET api/statistics/bookCount/:branch_id
// @desc    Returns the count of the books per branch.
// @access  Public
router.get("/bookCount/:branch_id", (req, res) => {
  Book.countDocuments({ branch: req.params.branch_id }).then(count => res.json(count ? count : 0))
    .catch(err => console.log(err));
});

// @route   GET api/statistics/allBookCount
// @desc    Returns the count of all books.
// @access  Public
router.get("/allBookCount", (req, res) => {
  Book.countDocuments({}).then(count => res.json(count ? count : 0))
    .catch(err => console.log(err));
});

// @route   GET api/statistics/issueCount
// @desc    Returns the count of all books issued by user.
// @access  Private
router.get("/issueCount", passport.authenticate("jwt", {session: false}), (req, res) => {
  
});

// @route   GET api/statistics/registrationCount/:branch_id
// @desc    Returns the count of registration for a branch.
// @access  Private
router.get("/registrationCount/:branch_id", passport.authenticate("jwt", {session: false}), (req, res) => {
  Registration.countDocuments({ branch: req.params.branch_id, isUserCreated: false, status: {$ne : "Application Rejected"}}).then(count => res.json(count ? count : 0))
    .catch(err => console.log(err));
});

// @route   GET api/statistics/requestCount/:branch_id
// @desc    Returns the count of requests for a branch.
// @access  Private
router.get("/requestCount/:branch_id", passport.authenticate("jwt", {session: false}), (req, res) => {
  Request.countDocuments({ branch: req.params.branch_id, isAdminApproved: false, isRejected: false }).then(count => res.json(count ? count : 0))
    .catch(err => console.log(err));
});

// export the router
module.exports = router;
