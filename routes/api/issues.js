// import dependencies
const express = require("express");
const passport = require("passport");

// initialize router
const router = express.Router();

// imports for database
const Issue = require("../../models/Issue");

// @route   GET api/issues/getCurrentBook/:user_id
// @desc    Get the current book issued to the user
// @access  Private
router.get("/getCurrentBook/:user_id", passport.authenticate("jwt", { session: false }), (req, res) => {
  Issue.findOne({ user: req.params.user_id, isReturned: false }).then(issue => {
    if (!issue) {
      return res.json(null);
    }

    return res.json(issue);
  })
  .catch((err) => {
    return res.status(400).json(err);
  });
});

// @route   POST api/issues/issueBook
// @desc    Issue a book to user
// @access  Private
router.post("/issueBook", passport.authenticate("jwt", { session: false }), (req, res) => {
  const issueFromDate = new Date();
  const issueTillDate = getDueDate();

  const newIssue = new Issue({
    "book": req.body.bookId,
    "user": req.user._id,
    "issuedFrom": issueFromDate,
    "issuedTill": issueTillDate,
  });

  newIssue
    .save()
    .then((issue) => res.json(issue))
    .catch((err) => console.log(err));
});

const getDueDate = (() => {
  currentDate = new Date();
  currentDate.setDate(currentDate.getDate() + 3);
  return currentDate;
});

// @route   POST api/issues/renewBook/:issue_id
// @desc    Renew the book issued by user
// @access  Private
router.post("/renewBook/:issue_id", passport.authenticate("jwt", { session: false }), (req, res) => {
  Issue.findOne({ _id: req.params.issue_id }).then(issue => {
    if (!issue) {
      return res.status(404).json(null);
    }
    
    issue.issuedTill += issue.issuedTill.setDate(issue.issuedTill.getDate() + 3);
    Issue.findByIdAndUpdate(
        { _id: req.params.issue_id },
        { $set: issue },
        { new: true }
      )
      .then(() => res.json("Done"))
      .catch(err => console.log(err));
  })
  .catch((err) => {
    return res.status(400).json(err);
  });
});

// @route   POST api/issues/returnBook/:issue_id
// @desc    Return the book issued to the user
// @access  Private
router.post("/returnBook/:issue_id", passport.authenticate("jwt", { session: false }), (req, res) => {
  Issue.findOne({ _id: req.params.issue_id }).then(issue => {
    if (!issue) {
      return res.status(404).json(null);
    }
    
    issue.issuedTill = new Date();
    issue.isReturned = true;
    Issue.findByIdAndUpdate(
        { _id: req.params.issue_id },
        { $set: issue },
        { new: true }
      )
      .then(() => res.json("Done"))
      .catch(err => console.log(err));
  })
  .catch((err) => {
    return res.status(400).json(err);
  });
});

// export the router
module.exports = router;
