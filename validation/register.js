// imports for the dependencies
const Validator = require("validator");
const isEmpty = require("./is-empty");

// export the validator for usage
module.exports = function validateRegisterInput(data) {
  let errors = {};

  // make sure the fields are present before checking
  data.email = !isEmpty(data.email) ? data.email : '';
  data.name = !isEmpty(data.name) ? data.name : '';
  data.branch = !isEmpty(data.branch) ? data.branch : '';

  // check for empty values
  if (Validator.isEmpty(data.email)) {
    errors.email = "Email field is required"
  }

  if (Validator.isEmpty(data.name)) {
    errors.name = "Name field is required"
  }

  if (Validator.isEmpty(data.branch)) {
    errors.branch = "Branch is required"
  }

  // check for other conditions if the fields are filled
  if (!Validator.isEmail(data.email) && (!errors.email || errors.email === "")) {
    errors.email = "Email is invalid"
  }

  return {
    errors,
    isValid: isEmpty(errors),
  }
}