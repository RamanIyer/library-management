// imports for the dependencies
const Validator = require("validator");
const isEmpty = require("./is-empty");

// export the validator for use
module.exports = function validateBookInput(data, isRequest) {
  let errors = {};

  // make sure the fields are present before checking
  data.name = !isEmpty(data.name) ? data.name : '';
  data.author = !isEmpty(data.author) ? data.author : '';
  data.isbn = !isEmpty(data.isbn) ? data.isbn : '';
  data.genre = !isEmpty(data.genre) ? data.genre : '';
  if (!isRequest) {
    data.quantity = !isEmpty(data.quantity) ? data.quantity : '';
    data.branch = !isEmpty(data.branch) ? data.branch : '';
  }

  // check for empty values
  if (Validator.isEmpty(data.name)) {
    errors.name = "Name field is required"
  }

  if (Validator.isEmpty(data.author)) {
    errors.author = "Author field is required"
  }

  if (Validator.isEmpty(data.isbn)) {
    errors.isbn = "ISBN field is required"
  }

  if (Validator.isEmpty(data.genre)) {
    errors.genre = "Genre field is required"
  }

  if (!isRequest && Validator.isEmpty(data.quantity)) {
    errors.quantity = "Quantity field is required"
  }

  if (!isRequest && data.branch === "Branch") {
    errors.branch = "Branch is required"
  }

  return {
    errors,
    isValid: isEmpty(errors),
  }
}
