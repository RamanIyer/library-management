// imports for the dependencies
const Validator = require("validator");
const isEmpty = require("./is-empty");

// export the validation for usage
module.exports = function validateBranchInput(data) {
  let errors = {};

  // make sure the fields are present before checking
  data.email = !isEmpty(data.email) ? data.email : '';
  data.name = !isEmpty(data.name) ? data.name : '';
  data.location = !isEmpty(data.location) ? data.location : '';
  data.admin = !isEmpty(data.admin) ? data.admin : '';
  data.phone = !isEmpty(data.phone) ? data.phone : '';

  // check for empty values
  if (Validator.isEmpty(data.email)) {
    errors.email = "Select an admin to fill email field"
  }

  if (Validator.isEmpty(data.name)) {
    errors.name = "Name field is required"
  }

  if (Validator.isEmpty(data.location)) {
    errors.location = "Location field is required"
  }

  if (Validator.isEmpty(data.admin)) {
    errors.admin = "Admin field is required"
  }

  if (Validator.isEmpty(data.phone)) {
    errors.phone = "Phone field is required"
  }

  // check for other conditions if the fields are filled
  if (!Validator.isEmail(data.email) && (!errors.email || errors.email === "")) {
    errors.email = "Email is invalid"
  }

  if (!Validator.isLength(data.phone.toString(), { min: 10, max: 10 }) &&
    (!errors.phone || errors.phone === "")) {
    errors.phone = "Phone number must be 10 digits"
  }

  return {
    errors,
    isValid: isEmpty(errors),
  }
}
