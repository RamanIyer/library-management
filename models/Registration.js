// import for dependencies
const mongoose = require("mongoose");

// initialize schema
const Schema = mongoose.Schema;

// create registrationSchema
const registrationSchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: true,
  },
  branch: {
    type: Schema.Types.ObjectId,
    ref: "branches",
  },
  status: {
    type: String,
    required: true,
  },
  isUserCreated: {
    type: Boolean,
    default: false,
  },
});

// export created schema
module.exports = Registration = mongoose.model("registrations", registrationSchema);
