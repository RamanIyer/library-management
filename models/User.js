// import for dependencies
const mongoose = require("mongoose");

// initialize schema
const Schema = mongoose.Schema;

// create userSchema
const userSchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
  isAdmin: {
    type: Boolean,
    default: false,
  },
  isSuperAdmin: {
    type: Boolean,
    default: false,
  },
  branch: {
    type: Schema.Types.ObjectId,
    ref: 'branches',
  },
});

// export created schema
module.exports = User = mongoose.model("users", userSchema);
