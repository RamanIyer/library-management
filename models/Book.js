// import for dependencies
const mongoose = require("mongoose");

// initialize schema
const Schema = mongoose.Schema;

// create bookSchema
const bookSchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  author: {
    type: String,
    required: true,
  },
  isbn: {
    type: String,
    required: true,
  },
  genre: {
    type: String,
    required: true,
  },
  quantity: {
    type: Number,
    required: true,
  },
  branch: {
    type: Schema.Types.ObjectId,
    ref: 'branches',
  },
});

// export the created model
module.exports = Book = mongoose.model("books", bookSchema);
