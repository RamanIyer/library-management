// import for dependencies
const mongoose = require("mongoose");

// initialize schema
const Schema = mongoose.Schema;

// create issueSchema
const issueSchema = new Schema({
  book: {
    type: Schema.Types.ObjectId,
    ref: "books",
  },
  user: {
    type: Schema.Types.ObjectId,
    ref: "users",
  },
  issuedFrom: {
    type: Date,
    required: true,
  },
  issuedTill: {
    type: Date,
    required: true,
  },
  isReturned: {
    type: Boolean,
    default: false,
  }
});

// export created schema
module.exports = Issue = mongoose.model("issues", issueSchema);