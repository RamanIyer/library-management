// import for dependencies
const mongoose = require("mongoose");

// initialize schema
const Schema = mongoose.Schema;

// create requestSchema
const requestSchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  author: {
    type: String,
    required: true,
  },
  isbn: {
    type: String,
    required: true,
  },
  genre: {
    type: String,
    required: true,
  },
  isAdminApproved: {
    type: Boolean,
    default: false,
  },
  isSuperAdminApproved: {
    type: Boolean,
    default: false,
  },
  isRejected: {
    type: Boolean,
    default: false,
  },
  branch: {
    type: Schema.Types.ObjectId,
    ref: 'branches',
  },
});

// export the created model
module.exports = Request = mongoose.model("requests", requestSchema);
