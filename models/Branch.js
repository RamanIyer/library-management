// import for dependencies
const mongoose = require("mongoose");

// initialize schema
const Schema = mongoose.Schema;

// create branchSchema
const branchSchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  location: {
    type: String,
    required: true,
  },
  admin: {
    type: Schema.Types.ObjectId,
    ref: 'users',
  },
  phone: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: true,
  },
});

// export the schema
module.exports = Branch = mongoose.model("branches", branchSchema);
